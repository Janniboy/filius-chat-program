/*
 ** This file is part of Filius, a network construction and simulation software.
 ** 
 ** Originally created at the University of Siegen, Institute "Didactics of
 ** Informatics and E-Learning" by a students' project group:
 **     members (2006-2007): 
 **         AndrÃ© Asschoff, Johannes Bade, Carsten Dittich, Thomas Gerding,
 **         Nadja HaÃ?ler, Ernst Johannes Klebert, Michell Weyer
 **     supervisors:
 **         Stefan Freischlad (maintainer until 2009), Peer Stechert
 ** Project is maintained since 2010 by Christian Eibl <filius@c.fameibl.de>
 **         and Stefan Freischlad
 ** Filius is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 2 of the License, or
 ** (at your option) version 3.
 ** 
 ** Filius is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied
 ** warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 ** PURPOSE. See the GNU General Public License for more details.
 ** 
 ** You should have received a copy of the GNU General Public License
 ** along with Filius.  If not, see <http://www.gnu.org/licenses/>.
 */
package filius.gui.anwendungssicht;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Observable;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import filius.software.clientserver.chat_client;

/**
 * <p>
 * Diese Klasse stellt die Benutzungsoberflaeche fuer das Client-Programm einer
 * einfachen Client-Server-Anwendung zur Verfuegung.
 * </p>
 * <p>
 * Nachrichten von der Anwendung werden nach dem Beobachtermuster durch die
 * Benachrichtigung der Beobachter angenommen und verarbeitet.
 * </p>
 */
public class GUIApplicationchat_clientWindow extends GUIApplicationWindow {

    private static final long serialVersionUID = 1L;
    
    private static List<Character> controlChars;

    /**
     * Textfeld fuer die Ausgabe gesendeter und empfangener Nachrichten sowie
     * fuer Fehlermeldungen
     */
    private JTextArea taAusgabe;

    /**
     * Textfeld fuer die Adresse des Servers, zu dem die Verbindung hergestellt
     * werden soll
     */
    private JTextField tfServerAdresse;

    /**
     * Textfeld zur Angabe des TCP-Ports, auf dem der Server auf eingehende
     * Nachrichten wartet
     */
    private JTextField tfServerPort;

    /**
     * Textfeld zur Angabe des Nutzernames der in der Chatanwendung verwendet
     * werden soll
     */
    private JTextField tfUsername;

    /** Textbereich zur Eingabe der Nachrichten */
    private JTextArea taSenden;

    /** Schaltflaeche zum initiieren des Verbindungsaufbaus */
    private JButton btVerbinden;

    /**
     * Schaltflaeche zum Senden einer zuvor eingegebenen Nachricht
     */
    private JButton btSenden;
    
    /**
     * Schaltflaeche zum Leeren des Ausgabetextbereichs
     */
    private JButton btLeeren;
    
    // for sending auth packet only once
    private boolean wasConnected;

    /**
     * Standard-Konstruktor, der automatisch zur Erzeugung der graphischen
     * Benutzungsoberflaeche fuer diese Anwendung aufgerufen wird.
     */
    public GUIApplicationchat_clientWindow(GUIDesktopPanel desktop, String appName) {
        super(desktop, appName);
        controlChars = ((chat_client) holeAnwendung()).controlChars;
        initialisiereKomponenten();
    }

    /** Methode zur Initialisierung der graphischen Komponenten */
    private void initialisiereKomponenten() {
        JPanel hauptPanel;
        JScrollPane scrollPane;
        Box hauptBox;
        Box hBox;
        JLabel label;

        hauptPanel = new JPanel(new BorderLayout());

        hauptBox = Box.createVerticalBox();
        hauptBox.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        hauptBox.add(Box.createVerticalStrut(5));

        hBox = Box.createHorizontalBox();
        label = new JLabel(messages.getString("clientbaustein_msg1"));
        label.setPreferredSize(new Dimension(140, label.getHeight()));
        hBox.add(label);

        tfServerAdresse = new JTextField();
        tfServerAdresse.setPreferredSize(new Dimension(100, 20));
        hBox.add(tfServerAdresse);
        hauptBox.add(hBox);
        hauptBox.add(Box.createVerticalStrut(5));

        hBox = Box.createHorizontalBox();
        label = new JLabel(messages.getString("clientbaustein_msg2"));
        label.setPreferredSize(new Dimension(140, label.getHeight()));
        hBox.add(label);

        tfServerPort = new JTextField();
        tfServerPort.setPreferredSize(new Dimension(55, 20));
        hBox.add(tfServerPort);
        
        hBox.add(Box.createHorizontalStrut(5));
        label = new JLabel("Username: ");
        label.setPreferredSize(new Dimension(85, label.getHeight()));
        hBox.add(label);

        tfUsername = new JTextField();
        tfUsername.setPreferredSize(new Dimension(55, 20));
        hBox.add(tfUsername);
        hauptBox.add(hBox);
        hauptBox.add(Box.createVerticalStrut(5));
        
        hBox = Box.createHorizontalBox();
        btVerbinden = new JButton(messages.getString("clientbaustein_msg3"));
        btVerbinden.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().equals("verbinden")) {
                    ((chat_client) holeAnwendung()).setZielIPAdresse(tfServerAdresse.getText());
                    ((chat_client) holeAnwendung()).setZielPort(Integer.parseInt(tfServerPort.getText()));
                    ((chat_client) holeAnwendung()).verbinden();
                } else {
                	((chat_client) holeAnwendung()).senden(controlChars.get(1)+tfUsername.getText());
                    ((chat_client) holeAnwendung()).trennen();
                }
                aktualisieren();
            }
        });
        hBox.add(btVerbinden);
        hauptBox.add(hBox);
        hauptBox.add(Box.createVerticalStrut(5));

        hBox = Box.createHorizontalBox();
        label = new JLabel(messages.getString("clientbaustein_msg4"));
        label.setPreferredSize(new Dimension(100, 20));
        hBox.add(label);
        hauptBox.add(hBox);

        taSenden = new JTextArea();
        scrollPane = new JScrollPane(taSenden);
        scrollPane.setPreferredSize(new Dimension(400, 50));
        hauptBox.add(scrollPane);
        
        hBox = Box.createHorizontalBox();
        label = new JLabel("");
        label.setPreferredSize(new Dimension(103, 20));
        hBox.add(label);
        hBox.add(Box.createHorizontalStrut(5));
        hBox.add(Box.createHorizontalGlue());
        
        btSenden = new JButton(messages.getString("clientbaustein_msg5"));
        btSenden.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ((chat_client) holeAnwendung()).senden("<" + tfUsername.getText() + "> " + taSenden.getText());
                taSenden.setText("");
            }
        });
        hBox.add(btSenden);
        hBox.add(Box.createHorizontalStrut(5));
    
        hBox.add(Box.createHorizontalGlue());
        btLeeren = new JButton("Chat Leeren");
        btLeeren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				taAusgabe.setText("");
			}
		});
        hBox.add(btLeeren);
        hauptBox.add(hBox);
        hauptBox.add(Box.createVerticalStrut(5));

        taAusgabe = new JTextArea();
        taAusgabe.setEditable(false);
        scrollPane = new JScrollPane(taAusgabe);
        scrollPane.setPreferredSize(new Dimension(400, 200));
        hauptBox.add(scrollPane);
        hauptBox.add(Box.createVerticalStrut(5));

        hauptPanel.add(hauptBox, BorderLayout.CENTER);

        getContentPane().add(hauptPanel);
        pack();

        aktualisieren();
    }

    /**
     * Methode zum aktualisieren der Komponenten der graphischen
     * Benutzungsoberflaeche in Abhaengigkeit vom Zustand der Anwendung
     */
    private void aktualisieren() {
        chat_client client;

        client = (chat_client) holeAnwendung();

        tfServerAdresse.setText(client.getZielIPAdresse());
        tfServerPort.setText("" + client.getZielPort());
        if (client.istVerbunden()) {
            btVerbinden.setText(messages.getString("clientbaustein_msg6"));
            btVerbinden.setActionCommand("trennen");
            btSenden.setEnabled(true);

            tfServerAdresse.setEditable(false);
            tfServerPort.setEditable(false);
            tfUsername.setEditable(false);
            
            if (!wasConnected) {
            	wasConnected = true;
            	// send auth packet
            	((chat_client) holeAnwendung()).senden("\uE001"+tfUsername.getText());
            }
        } else {
            btVerbinden.setText(messages.getString("clientbaustein_msg3"));
            btVerbinden.setActionCommand("verbinden");
            btSenden.setEnabled(false);

            tfServerAdresse.setEditable(true);
            tfServerPort.setEditable(true);
            tfUsername.setEditable(true);
            
            wasConnected = false;
        }
    }

    /**
     * Diese Methode wird automatisch ausgefuehrt, wenn eine Nachricht an den
     * Beobachter der Anwendung gesendet wird. Der Parameter arg enthaelt die
     * Nachricht, die von der Anwendung verschickt wurde.
     */
    public void update(Observable o, Object arg) {
        if (arg != null) {
        	if (arg.toString().equals(messages.getString("sw_clientbaustein_msg2"))) {
        		// suppress connection message "Verbindung hergestellt"
        		//this.taAusgabe.append(arg.toString() + "\n");
        	} else if (arg.toString().equals(messages.getString("sw_clientbaustein_msg3"))) {
        		// suppress disconnect message "Verbindung getrennt"
        		//this.taAusgabe.append(arg.toString() + "\n");
        	} else if (arg.toString().startsWith(messages.getString("sw_clientbaustein_msg5"))) {
        		// suppress 2nd disconnect message "Socket zu IP:PORT geschlossen"
        		//this.taAusgabe.append(arg.toString() + "\n");
        	} else {
        		this.taAusgabe.append(arg.toString() + "\n");
        	}
    	}
        aktualisieren();
    }
}
