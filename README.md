# Funktionalität
## aktuelle
- mehrere Clients können mit Server verbinden
- Server & Client haben einen "Clear" Button um den Chat nur für sich zu leeren
- Jeder Client kann sich einen Username geben, welcher dann im Chat angezeigt wird (ein Ändern des Username betrifft nur zukünftige Nachrichten)
- Benachrichtigung wenn ein Nutzer den Chat betritt oder verlässt

## zukünftige
(das ist Liste an Ideen, keine TODO)
UPDATE: Ich habe hier geschafft was ich wollte, daher wird es warscheinlich keine Updates geben. Falls jemand anderes Lust hat Code beizusteuern, kann er/sie/.* gerne eine Merge-Request erstellen. (oder einfach forken :)

### /befehle
verschiedene Befehle wie `/help`, `/list` (um alle Teilnehmer aufzulisten)

### Konten System
Nutzer erstellen ein Konto beim Server (oder ein Konto wird auf dem Server angelegt) und können sich dann mit Nutzername und Passwort anmelden
(vielleicht ein bisschen Overkill)

### Einstellungen
Eine GUI um Einstellungen zu ändern (z.B.: ob "Verbindung hergestellt" oder "Nutzer MEIN_NAME@MEINE_IP ..." angezeigt werden soll, Themes, ...)

# Notizen
## [Issue#34](https://gitlab.com/filius1/filius/-/issues/34)
Aufgrund dieses Bugs? befindet sich in der Klasse `chat_serverMitarbeiter.java` eine minimal abgewandelte (siehe [Issue#34](https://gitlab.com/filius1/filius/-/issues/34)) Kopie der Methode `ServerMitarbeiter.run()`.

## [Issue#35](https://gitlab.com/filius1/filius/-/issues/35)
Aufgrund dieses Bugs, wurde in der Methode `chat_client.trennen()`, `this.socket = null;` entfernt. (Könnte zu Bugs führen, ist ohne Modifikation von Filius aber der einzige Weg)

## Dev Environment
Einfach das Filius Repo klonen und die in diesem Repo enthaltenen Dateien entsprechend ihres Package-Paths einfügen.
Danach kann das ganze über den Git-Smart-Import von Eclipse geladen und modifiziert werden (zum testen trotzdem wie Normal via Software Assistent laden, sonst kann es zu Bugs kommen).

## Bugs und Wichtiges, beim rekompilieren von Filius mit diesem Chat Program
Hier muss darauf geachtet werden, dass die Konfigurationsdateien (`/src/main/resources/config/Desktop_*.txt`) für die entsprechenden Sprachen bearbeitet werden, um die Programme anzuzeigen.
Desweiteren werden die hinzugefügten Programme unter Umständen nicht auf dem Desktop angezeigt (derzeit weiß Ich von keinem Fix).

# Bugs
Keine! (Bekannten...)
