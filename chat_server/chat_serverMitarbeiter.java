/*
 ** This file is part of Filius, a network construction and simulation software.
 ** 
 ** Originally created at the University of Siegen, Institute "Didactics of
 ** Informatics and E-Learning" by a students' project group:
 **     members (2006-2007): 
 **         AndrÃ© Asschoff, Johannes Bade, Carsten Dittich, Thomas Gerding,
 **         Nadja HaÃ?ler, Ernst Johannes Klebert, Michell Weyer
 **     supervisors:
 **         Stefan Freischlad (maintainer until 2009), Peer Stechert
 ** Project is maintained since 2010 by Christian Eibl <filius@c.fameibl.de>
 **         and Stefan Freischlad
 ** Filius is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 2 of the License, or
 ** (at your option) version 3.
 ** 
 ** Filius is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied
 ** warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 ** PURPOSE. See the GNU General Public License for more details.
 ** 
 ** You should have received a copy of the GNU General Public License
 ** along with Filius.  If not, see <http://www.gnu.org/licenses/>.
 */
package filius.software.clientserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import filius.exception.VerbindungsException;
import filius.software.transportschicht.Socket;

/**
 * <p>
 * In dieser Klasse erfolgt die Verarbeitung von eingehenden Nachrichten an
 * einen Server.
 * </p>
 * <p>
 * Die Oberklasse <code>ServerMitarbeiter</code> erbt von der Klasse Thread. In
 * der <code>run()</code>-Methode der
 * Oberklasse wird der Socket auf eingehende Nachrichten ueberwacht. Sobald eine
 * Nachricht eintrifft, wird diese an die
 * Methode <code>verarbeiteNachricht(String)</code> zur weiteren Verarbeitung
 * weiter gegeben. Ausserdem wird dort der
 * Socket automatisch geschlossen, wenn das Client-Programm den Verbindungsabbau
 * initiiert.
 * </p>
 * <p>
 * In dieser Klasse sollte nur die Methode <code>senden(String)</code> des
 * Sockets verwendet werden!
 * </p>
 */

public class chat_serverMitarbeiter extends ServerMitarbeiter {
    private static Logger LOG = LoggerFactory.getLogger(chat_serverMitarbeiter.class);
    
    private chat_server realServer;

    /**
     * Standard-Konstruktor. Wenn der Server auf einem bestimmten Port auf
     * eingehende Verbindungen warten soll, muss die
     * Port-Nummer hier mit <code>setPort(int)</code> initialisiert werden!
     */
    public chat_serverMitarbeiter(ServerAnwendung server, chat_server realServer,Socket socket) {
        super(server, socket);
        this.realServer = realServer;
    }
    
    public void run() {
        LOG.trace("INVOKED (" + this.hashCode() + ", T" + this.getId() + ") " + getClass()
                + " (ServerMitarbeiter), run()");
        String nachricht = null;
        while (running) {
            try {
                if (socket.istVerbunden()) {
                    nachricht = socket.empfangen(Long.MAX_VALUE);
                }

                if (nachricht != null) {
                    verarbeiteNachricht(nachricht);
                } else if (socket != null) {
                    socket.schliessen();
                    running = false;

                    server.benachrichtigeBeobachter(
                            messages.getString("sw_servermitarbeiter_msg1") + " " + socket.holeZielIPAdresse() + ":"
                                    + socket.holeZielPort() + " " + messages.getString("sw_servermitarbeiter_msg2"));
                }
                nachricht = null;
            } catch (VerbindungsException e) {
                LOG.debug("", e);
                server.benachrichtigeBeobachter(e.getMessage());
                socket.beenden();
                running = false;
                server.entferneMitarbeiter(this);
            } catch (Exception e) {
                LOG.debug("", e);
                server.benachrichtigeBeobachter(e.getMessage());
                socket.schliessen();
                running = false;
                server.entferneMitarbeiter(this);
            }
        }
    }

    /**
     * Methode, die automatisch aufgerufen wird, wenn eine neue Nachricht eintrifft.
     * Hier erfolgt die Verarbeitung der
     * eingehenden Nachricht.
     */
    protected void verarbeiteNachricht(String nachricht) {
        LOG.trace("INVOKED (" + this.hashCode() + ", T" + this.getId() + ") " + getClass()
                + " (chat_serverMitarbeiter), verarbeiteNachricht(" + nachricht + ")");
        try {
        	this.realServer.messageReceived(nachricht, this);
        	// Default Code
            //socket.senden(nachricht);
            //server.benachrichtigeBeobachter("<<" + nachricht);
        } catch (Exception e) {
            LOG.debug("", e);
            server.benachrichtigeBeobachter(e.getMessage());
        }
    }
    
    public void sendMessage(String Message) {
    	try {
    		socket.senden(Message);
    	} catch (Exception e){
    		LOG.debug("", e);
    		server.benachrichtigeBeobachter(e.getMessage());
    	}
    }
}
